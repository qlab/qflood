#### If you have questions or issues you can contact me here:
* Matrix: @qbin:matrix.org
* Discord: #drkurbelstrahl
* Mastodon: @qbinary@mastodon.social

> English version
# A flood of pixels! – Evoke 2023
Hi! Cool that you found your way here!

### What is this?
This is a canvas on which you can set pixels using TCP connection!  
Simply establish a TCP connection to the specified address and port, and start pixeling!  

The whole thing runs intentionally on an old "RaspberryPi 1 B+". Would be boring if the whole thing would be responsive right? :D

### Rules
1. botting is allowed of course!
2. ready-made tools are **not welcome**! party code encouraged!
3. have fun!
4. do not be afraid to overwrite others pixels! The stronger client wins!

### Suggestions
1. plot your logo!
2. make a little animation!
3. build a botnet out of all your members.... ( the poor network :] )

### Technical
* [**Protocol documentation here!**](https://github.com/defnull/pixelflut)
* The server runs a slightly [modified version of **pixelnuke**](https://codeberg.org/qlab/qflood/)
    * [(pixelnuke)](https://github.com/defnull/pixelflut/tree/master/pixelnuke)

> German version
# Eine Pixelflut!  – Evoke 2023
Hi! Cool, dass du hier her gefunden hast.

### Was ist das?
Das ist eine Leinwand, auf der man mittels TCP verbindung pixel setzen kann!  
Einfach eine TCP Verbindung zur angegebenen Adresse und Port aufbauen, und lospixeln!  

Das ganze läuft absichtlich auf einem alten "RaspberryPi 1 B+". Wäre ja langweilig, wenn das ganze responsive wäre :D

### Regeln
1. Botting ist selbstverständlich erlaubt!
2. Fertige Tools sind **unerwünscht**! party code ist cooler!
3. Habt spaß!

### Annregungen
1. Plottet euer Logo!
2. Macht eine kleine Animation!
3. Baut euch ein Botnet aus all euren Mitgliedern auf... ( das arme Netwerk :] )
4. Scheut euch nicht davor andere Sachen zu überschreiben! Der stärkere Client gewinnt!

### Technisches
* [**Protokoll findet ihr hier!**](https://github.com/defnull/pixelflut)
* Auf dem Server läuft eine leicht [modifizierte Version von **pixelnuke**](https://codeberg.org/qlab/qflood/)
    *  [(pixelnuke)](https://github.com/defnull/pixelflut/tree/master/pixelnuke)